# Currently: Broccoli (day 1 - 19:00)
https://card10.badge.events.ccc.de/release/card10-v1.2.zip

# Update the firmware release on your card10
* download the current firmware `.zip` file to your computer
* extract it
* put your card10 into [USB storage mode](/en/gettingstarted#usb-storage)
* copy over the files you just extracted directly into the main folder
* eject your device (if you're doing this in the command line: don't forget the `sync` on linux)
* switch your card10 on and off again

# Previous Releases

## Asparagus
As of Day 1, 18:00
Asparagus is the firmware that is loaded on the card10s handed out on day 1 from 18:00
There are currently no previous releases